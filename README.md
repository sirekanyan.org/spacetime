# Spacetime

Simple but awesome stopwatch timer app with cool [nasa](https://github.com/sirekanian/apod) wallpapers

<picture>
  <img width="200" src="app/src/main/play/listings/en-US/graphics/phone-screenshots/1.png">
</picture>

<picture>
  <img width="200" src="app/src/main/play/listings/en-US/graphics/phone-screenshots/2.png">
</picture>

<picture>
  <img width="200" src="app/src/main/play/listings/en-US/graphics/phone-screenshots/3.png">
</picture>

<a href='https://play.google.com/store/apps/details?id=com.sirekanian.spacetime'><img height='100' alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
